<!-- Services -->
<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Services</h2>
                <h3 class="section-subheading text-muted">Experienced people inside Mein are ready to serve you</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">E-Commerce</h4>
                <p class="text-muted">Open online business channel, start your own marketplace or online shop.</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Microsite</h4>
                <p class="text-muted">Plan your next marketing strategy with online campaign platform.</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-area-chart fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Information System</h4>
                <p class="text-muted">We know manually managing system is hard. Here, we can help you to create a platform to solve it.</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">IOS Development</h4>
                <p class="text-muted">Need an IOS apps, just call us and send inquiry.</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-android fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Mobile apps</h4>
                <p class="text-muted">Mobile apps is one of the best channel for support your business. Do you plan to create one?</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Company Portal</h4>
                <p class="text-muted">Explain about your business / company with wonderful websites.</p>
            </div>
        </div>
    </div>
</section>