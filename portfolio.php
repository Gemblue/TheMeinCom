<!-- Portfolio Grid -->
<section class="bg-light" id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Portfolio</h2>
                <h3 class="section-subheading text-muted">Some of our works, let's check out what we have done.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#SooqyModal">
                    
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    
                    <img class="img-fluid" src="img/portfolio/sooqy/screenshot.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Sooqy Travel</h4>
                    <p class="text-muted">
                        Online platform for booking hotel and airlines.<br/>
                    </p>
                </div>
                
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#LoveHurtsModal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img class="img-fluid" src="img/portfolio/lovehurts/screenshot.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>#Lovehurts Microsite</h4>
                    <p class="text-muted">Microsite campaign platform for Zurich Insurance</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#MLDJazz">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img class="img-fluid" src="img/portfolio/mldjazzwanted/screenshot.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>MLDJazzWanted</h4>
                    <p class="text-muted">Microsite campagin platform for Djarum MLD</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#IdaPortal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img class="img-fluid" src="img/portfolio/ida/screenshot.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Ida Company Profile</h4>
                    <p class="text-muted">Online Company Profile for Indonesia digital association</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#Eproc">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img class="img-fluid" src="img/portfolio/eproc/screenshot.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>E-Proc</h4>
                    <p class="text-muted">Online procurement system for one of the biggest Toyota Dealer in Indonesia</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#Ukbi">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img class="img-fluid" src="img/portfolio/ukbi/screenshot.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>UKBI</h4>
                    <p class="text-muted">UKBI is Indonesian language online learning center platform.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#Retinad">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img class="img-fluid" src="https://i.imgur.com/VIWPIZw.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Retinad Wifi</h4>
                    <p class="text-muted">Wifi Gateway Platform</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#Meta">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img class="img-fluid" src="https://i.imgur.com/CC0W5fN.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>Uji Meta Cognitif</h4>
                    <p class="text-muted">Quiz & Survey Platform</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a class="portfolio-link" data-toggle="modal" href="#Kuni">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img class="img-fluid" src="https://i.imgur.com/FGscQMR.png" alt="">
                </a>
                <div class="portfolio-caption">
                    <h4>KuniKita</h4>
                    <p class="text-muted">E-Commerce</p>
                </div>
            </div
        </div>
    </div>
</section>

<div class="portfolio-modal modal fade" id="Meta" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">Uji Meta Cognitif</h2>
                        <p class="item-intro text-muted">Quiz & Survey Platform.</p>
                        <img class="img-fluid d-block mx-auto" src="https://i.imgur.com/CC0W5fN.png" alt="">
                        <p>Sistem Uji Meta Cognitif adalah sistem pengujian cognitif untuk siswa sekolah. Terdapat beberapa fitur seperti quiz, dashboard, management sekolah, siswa dan lain lain.</p>
                        <ul class="list-inline">
                            <li>Date: January 2016</li>
                            <li>Client: Universitas Indonesia</li>
                            <li>Category: Quiz & Survey Platform</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="Kuni" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">Kunikita</h2>
                        <p class="item-intro text-muted">E-Commerce</p>
                        <img class="img-fluid d-block mx-auto" src="https://i.imgur.com/FGscQMR.png" alt="">
                        <p>Kunikita adalah sebuah UKM terdepan di kota Garut. Perusahaan tersebut merupakan perusahaan yang bergerak di bidang makanan dan fashion. Kami membangun website E-Commerce untuk Kunikita yang menghandle otomasi proses bisnis dan etalase digital juga sistem reseller nya</p>
                        <ul class="list-inline">
                            <li>Date: January 2019</li>
                            <li>Client: Kunikita</li>
                            <li>Category: E-Commerce</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="Retinad" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">Retinad Wifi</h2>
                        <p class="item-intro text-muted">Wifi Gateway Platform.</p>
                        <img class="img-fluid d-block mx-auto" src="https://i.imgur.com/64iIRoV.png" alt="">
                        <p>We develop a wifi gateway that connected to RADIUS and Mikrotik. It can handle wifi business process like voucher, ads, accounts, and many more</p>
                        <ul class="list-inline">
                            <li>Date: January 2016</li>
                            <li>Client: PT. Global Kreasi Mandiri</li>
                            <li>Category: Wifi Platform</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="SooqyModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">Sooqy Travel</h2>
                        <p class="item-intro text-muted">Online travel platform.</p>
                        <img class="img-fluid d-block mx-auto" src="img/portfolio/sooqy/animation.gif" alt="">
                        <p>We develop a website for booking hotels and arlines. There are two version, mobile and website channel.</p>
                        <ul class="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: PT. Sooqy Global International</li>
                            <li>Category: Travel Platform</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="LoveHurtsModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">#LoveHurts Microsite</h2>
                        <p class="item-intro text-muted">Online Campaign Microsite for Zurich Marketing</p>
                        <img class="img-fluid d-block mx-auto" src="img/portfolio/lovehurts/animation.gif" alt="">
                        <p>We develop a website for Zurich campaign. It is a website that enable user to generate a meme about her life and share to others.</p>
                        <ul class="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: PT.Global Kreasi Mandiri</li>
                            <li>Category: Microsite Campaign</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="MLDJazz" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">MLDJazzWanted Microsite</h2>
                        <p class="item-intro text-muted">Online Campaign Microsite for Djarum MLD</p>
                        <img class="img-fluid d-block mx-auto" src="img/portfolio/mldjazzwanted/animation.gif" alt="">
                        <p>We develop a website for Djarum MLD campaign. It is a website that enable user to upload their Jazz creation and vote another.</p>
                        <ul class="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: PT.Global Kreasi Mandiri</li>
                            <li>Category: Microsite Campaign</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="IdaPortal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">Ida Company Profile</h2>
                        <p class="item-intro text-muted">Online Company Profile for Indonesia digital association</p>
                        <img class="img-fluid d-block mx-auto" src="img/portfolio/ida/animation.gif" alt="">
                        <p>We develop a website for Indonesia digital association. It contains jobs board, event publishing, and many more.</p>
                        <ul class="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: PT.Global Kreasi Mandiri</li>
                            <li>Category: Microsite Campaign</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="Eproc" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">E-Proc System</h2>
                        <p class="item-intro text-muted">E-Procurement for PT. AGUNG TOYOTA</p>
                        <img class="img-fluid d-block mx-auto" src="img/portfolio/eproc/animation.gif" alt="">
                        <p>Online procurement system for one of the biggest Toyota Dealer in Indonesia. It has many feature like procurement flow, disposition, airwaybill, and statistics system.  It also connected to main Agung Toyota System with API Integration.</p>
                        <ul class="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: PT. AGUNG TOYOTA</li>
                            <li>Category: Information System</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="Ukbi" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="modal-body">
                        <h2 class="text-uppercase">UKBI (Uji Kemahiran Berbahasa Indonesia)</h2>
                        <p class="item-intro text-muted">E-Learning Platform</p>
                        <img class="img-fluid d-block mx-auto" src="img/portfolio/ukbi/animation.gif" alt="">
                        <p>E-Learning platform for training Indonesian Language.</p>
                        <ul class="list-inline">
                            <li>Date: January 2017</li>
                            <li>Client: Badan Balai Bahasa (Kemdikbud)</li>
                            <li>Category: E-Learning</li>
                        </ul>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">
                            <i class="fa fa-times"></i>
                            Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
