<?php include('header.php');?>
    
    <?php include('navbar.php');?>
    
    <!-- Header -->
    <header class="masthead">
        <div class="container">
            <div class="intro-text">
                <div class="intro-heading">
                    We Solve Your Problem <br/>With <span class="typed"></span>
                </div>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">LEARN MORE</a>
            </div>
        </div>
    </header>
    
    <?php include('services.php');?>
    
    <?php include('portfolio.php');?>

    <?php include('about.php');?>
    
    <?php include('contact.php');?>

<?php include('footer.php');?>